shebang\_change\_filetype.vim
=============================

This plugin re-runs filetype detection if a shebang `#!` is added or changed in
the buffer.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
