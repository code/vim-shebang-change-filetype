"
" shebang_change_filetype.vim: Re-run filetype detection if a shebang #! is
" added or changed in the buffer.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_shebang_change_filetype') || &compatible || v:version < 700
  finish
endif
let loaded_shebang_change_filetype = 1

" Set up hook for before writes to check the buffer for new shebangs
augroup shebang_change_filetype
  autocmd!
  autocmd InsertLeave *
        \ call shebang_change_filetype#()
  if exists('##TextChanged')
    autocmd TextChanged *
          \ doautocmd shebang_change_filetype InsertLeave
  endif
augroup END
