" Check whether the first line was changed and looks like a shebang, and if
" so, re-run filetype detection
function! shebang_change_filetype#() abort
  if line("'[") == 1
        \ && strpart(getline(1), 0, 2) ==# '#!'
    doautocmd filetypedetect BufRead
  endif
endfunction
